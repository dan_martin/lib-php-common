<?php
/**
 * This file is part of Kisma(tm).
 *
 * Kisma(tm) <https://github.com/kisma/kisma>
 * Copyright 2009-2013 Jerry Ablan <jerryablan@gmail.com>
 *
 * Kisma(tm) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kisma(tm) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Kisma(tm).  If not, see <http://www.gnu.org/licenses/>.
 */
namespace DreamFactory\Common\Utility;

use Kisma\Core\Components\LineWriter;
use Kisma\Core\Exceptions\FileSystemException;
use Kisma\Core\Interfaces\EscapeStyle;

/**
 * LineWriter.php
 * Based on the Kisma LineWriter but converts object and array values to json
 */
class CommonLineWriter extends LineWriter
{
	/**
	 * @param array $data
	 *
	 * @throws \Kisma\Core\Exceptions\FileSystemException
	 */
	protected function _write( $data )
	{
		if ( null === $this->_handle )
		{
			throw new FileSystemException( 'The file must be open to write data.' );
		}

		$_values = array();

		foreach ( $data as $_value )
		{
			// handle objects and array non-conformist to csv output
			if ( is_array( $_value ) || is_object( $_value ) )
			{
				$_value = json_encode( $_value );
			}

			if ( null === $_value )
			{
				if ( null !== $this->_nullValue )
				{
					$_values[] = $this->_nullValue;
					continue;
				}

				$_value = '';
			}

			if ( '' === $_value )
			{
				$_values[] = !$this->_wrapWhitespace ? '' : ( $this->_enclosure . $this->_enclosure );
				continue;
			}

			if ( $this->_lazyWrap && false === strpos( $_value, $this->_separator ) &&
				 ( $this->_enclosure === '' || false === strpos( $_value, $this->_enclosure ) )
			)
			{
				$_values[] = $_value;
				continue;
			}

			switch ( $this->_escapeStyle )
			{
				case EscapeStyle::DOUBLED:
					$_value = str_replace( $this->_enclosure, $this->_enclosure . $this->_enclosure, $_value );
					break;
				case EscapeStyle::SLASHED:
					$_value = str_replace( $this->_enclosure, '\\' . $this->_enclosure, str_replace( '\\', '\\\\', $_value ) );
					break;
			}

			$_values[] = $this->_enclosure . $_value . $this->_enclosure;
		}

		$_line = implode( $this->_separator, $_values );

		if ( !$this->_appendEOL )
		{
			$_line .= $this->_lineBreak;
		}
		else if ( $this->_linesOut > 0 )
		{
			$_line = $this->_lineBreak . $_line;
		}

		if ( false === ( $_byteCount = fwrite( $this->_handle, $_line ) ) )
		{
			throw new FileSystemException( 'Error writing to file: ' . $this->_fileName );
		}

		if ( $_byteCount != mb_strlen( $_line ) )
		{
			throw new FileSystemException( 'Failed to write entire buffer to file: ' . $this->_fileName );
		}

		$this->_linesOut++;
	}
}
